#About

This is a video game project started for my term paper about 2D game animation technologies. The development will be continued for my Bachelor thesis.

It is planned to be a 2D role playing adventure game for Windows PC.

Created by a final year Vytautas Magnus university (VMU) student:

* Sandra Pučkoriūtė - art, programming, story


## Screenshots

![Idle](https://i.imgur.com/CJJMCVN.png)
![Walking animation](https://i.imgur.com/BeZQWTi.png)

![Picking up items](https://i.imgur.com/vg6omM2.png)
![Toggling the inventory](https://i.imgur.com/HOlk11Y.png)

## Tools

* Unity (C#) - game creation, programming

* Paint Tool SAI, Photoshop CC 2018 - art