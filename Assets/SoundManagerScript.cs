﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour {

    public static AudioClip coinpick;
    static AudioSource audioSrc;

	// Use this for initialization
	void Start () {
        coinpick = Resources.Load<AudioClip>("coinpick");
        audioSrc = GetComponent<AudioSource>();
		
	}
	
	public static void playSound()
    {
        audioSrc.PlayOneShot(coinpick);
    }
}
