﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class MoveScript : MonoBehaviour
{

    private Rigidbody2D rb;
    private float dirX;
    public float maxSpeed = 2.5f;
    bool facingRight = true;

    Animator anim;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        dirX = CrossPlatformInputManager.GetAxis("Horizontal") * maxSpeed;
        anim.SetFloat("Speed", Mathf.Abs(dirX));

        Vector2 vel = new Vector2(dirX, rb.velocity.y);

        if (dirX > 0 && !facingRight)
        {
            Flip();
        }
        else if (dirX < 0 && facingRight)
        {
            Flip();
        }

        rb.velocity = vel;
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
