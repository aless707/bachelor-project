﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopControlScript : MonoBehaviour
{
    public Text candyText;
    public int candyPrice;
    public Button buyButton;

    public static AudioClip buycandy;
    static AudioSource audioSrc;

    void Start()
    {
        candyText.text = "Price: " + candyPrice.ToString() + " coins";

        buycandy = Resources.Load<AudioClip>("success");
        audioSrc = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

        if (ScoreTextScript.coinAmount >= candyPrice)
        {
            buyButton.interactable = true;
        }
        else
        {
            buyButton.interactable = false;
        }
    }

    public void buyCandy()
    {
        if (ScoreTextScript.coinAmount >= candyPrice)
        {
            ScoreTextScript.coinAmount -= candyPrice;
            candyText.text = "Sold!";
            audioSrc.PlayOneShot(buycandy);
            buyButton.gameObject.SetActive(false);
            //MoveScript.maxSpeed *= 2;
        }
    }
}
